from django.shortcuts import render
from django.http import HttpResponse
from .forms import StatesForm
from .models import Books, States
# Create your views here.

def index(request): 
    books = Books.objects.all()
    ans = request.GET.get('States')
    selected_state = States.objects.all().filter(statecode=ans)
    state_data = list(selected_state.values())
    notavail = "notavail"
    if state_data:
        selected_state_books = state_data[0]['sportsbooks']
        books_list = selected_state_books.split(',')
        books_data = Books.objects.all().filter(bookkeepername__in = books_list)
        return render(request, 'website/index3.html', {
            'Books' : books_data,
            'SS' : books_data
        })
    else:
        return render(request, 'website/index3.html', {
            'Books' : books,
            'notavail' : notavail
            })


def index2(request):
    return render(request, 'website/index.html')



    # resume video at 53:00